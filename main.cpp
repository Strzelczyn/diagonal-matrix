#include <iostream>

int main() {
  int size = 5;
  int** Matrix = new int*[size];
  for (int i = 0; i < size; ++i) {
    Matrix[i] = new int[size];
    for (int j = 0; j < size; ++j) {
      if (i == j) {
        Matrix[i][j] = 1;
      } else {
        Matrix[i][j] = 0;
      }
      std::cout << Matrix[i][j];
    }
    std::cout << std::endl;
  }

  for (int i = 0; i < size; ++i) {
    delete[] Matrix[i];
  }
  delete[] Matrix;
}
